package se.tekniskfysik.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import se.tekniskfysik.R;
import se.tekniskfysik.ui.News;
import se.tekniskfysik.ui.newsAdapter;

public class RSSFeedAsyncTask extends AsyncTask<RSSSettings, Void, List<News>> {
    // Variables fed from the parent fragment

    private int maxElements; // Maximum number of elements. Populated from settings
    private final static int MAX_STRING_LENGTH = 200; // Max length of description before truncation
    public List<News> news;
    public newsAdapter adapter;
    public Context context;
    public SwipeRefreshLayout swipeContainer;

    @Override
    protected void onPreExecute() {
        // Show spinning update arrow when first run
        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
            }
        });
    }

    @Override
    protected List<News> doInBackground(RSSSettings... settings) {
        maxElements = settings[0].maxElements;
        String url = settings[0].url;
        List<News> parsedFeed = null; // Initialise feed
        String rssFeed;

        try {
            rssFeed = getFeed(url);

            // Make sure that we actually were able to get the feed
            if(!isCancelled()) {
                parsedFeed = parseFeed(rssFeed);
            } else {
                return null;
            }
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }

        return parsedFeed;
    }

    private String getFeed(String url) {
        String line; // Store each line temporarely
        StringBuilder feed = new StringBuilder(); // Used to append each line to a string

        try {
            URL feedURL = new URL(url);
            URLConnection conn = feedURL.openConnection();

            // Set read timeout to 10 seconds
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            while ((line = in.readLine()) != null) {
                feed.append(line).append("\n"); // Append every new line and also a newline character
            }

            in.close(); // Close buffer
        } catch (IOException e) {
            e.printStackTrace();
        }

        return feed.toString();
    }

    // Method that parses the XML tags
    private List<News> parseFeed(String rawFeed) throws XmlPullParserException, IOException {
        List<News> rssItems = new ArrayList<>();
        String contents = null; // Store contents of tags
        String title = null;
        String pubDate = null;
        String description = null;
        String link = null;
        int counter = 0;

        // For changing date format
        Date dateParse;
        SimpleDateFormat input = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z", Locale.US);
        String output = "yyyyMMdd HH:dd";
        SimpleDateFormat sdf = new SimpleDateFormat(output, Locale.US);

        // Create a new instance of the factory
        XmlPullParserFactory xmlFactory = XmlPullParserFactory.newInstance();
        XmlPullParser parser = xmlFactory.newPullParser();
        parser.setInput(new StringReader(rawFeed));
        int eventType = parser.getEventType();
        while(eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    break;

                case XmlPullParser.TEXT:
                    contents = parser.getText();
                    break;

                case XmlPullParser.END_TAG:
                    if (parser.getName().equalsIgnoreCase("title")) {
                        title = contents;
                    } else if(parser.getName().equalsIgnoreCase("pubDate")) {
                        pubDate = contents;
                        try {
                            dateParse = input.parse(pubDate);
                            pubDate = sdf.format(dateParse);
                        } catch (ParseException e) {
                            Log.e("RSS Date Parse", "Could not parse date", e);
                        }
                    } else if(parser.getName().equalsIgnoreCase("description")) {
                        if(contents != null) {
                            // Remove HTML-tags from the description
                            // (char) 65532 is a dashed box with the word "obj" that appears when
                            // HTML-tags are removed from certain entities
                            contents = Html.fromHtml(contents).toString()
                                    .replace((char) 65532, (char) 32).trim();

                            // If the string is too long, truncate
                            if(contents.length() > MAX_STRING_LENGTH) {
                                contents = contents.substring(0, MAX_STRING_LENGTH) +
                                        context.getString(R.string.too_long);
                            }
                        }

                        description = contents;
                    } else if(parser.getName().equalsIgnoreCase("link")) {
                        link = contents;
                    } else if(parser.getName().equalsIgnoreCase("item")) {
                        // Add to list when we reach </item>
                        rssItems.add(new News(title, description, pubDate, link));

                        counter++; // Increase counter
                    }

                    break;
            }

            // Move to the next element
            eventType = parser.next();

            if(counter >= maxElements) {
                break; // End while-loop
            }
        }

        return rssItems;
    }

    @Override
    protected void onPostExecute(List<News> rssFeed) {
        if(rssFeed != null) {
            // Remove all old elements before the new ones are added to avoid duplicates
            news.clear();
            news.addAll(rssFeed);
        } else { // No news or could not update
            news.add(new News(context.getResources().getString(R.string.no_news),
                    context.getResources().getString(R.string.no_news_explanation), null, null));
        }

        // If we used pull to refresh, remove the spinning update arrow
        if(swipeContainer != null) {
            swipeContainer.setRefreshing(false);
        }

        // Update recycler view
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onCancelled(List<News> rssFeed) {
        if(swipeContainer != null) {
            // If pull to refresh and we suddenly don't get a result, show what we had before
            if(!(swipeContainer.isRefreshing() && rssFeed == null)) {
                news.add(new News(context.getResources().getString(R.string.no_news),
                        context.getResources().getString(R.string.no_news_explanation), null, null));
            }

            swipeContainer.setRefreshing(false);
        } else { // No news
            news.add(new News(context.getResources().getString(R.string.no_news),
                    context.getResources().getString(R.string.no_news_explanation), null, null));
        }

        // Update recycler view
        adapter.notifyDataSetChanged();
    }
}
