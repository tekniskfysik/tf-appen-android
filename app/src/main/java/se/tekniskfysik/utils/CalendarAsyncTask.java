package se.tekniskfysik.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.util.Calendars;


import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import se.tekniskfysik.R;
import se.tekniskfysik.ui.CalendarCard;
import se.tekniskfysik.ui.CalendarCardAdapter;

public class CalendarAsyncTask extends AsyncTask<CalendarSettings, Void, List<CalendarCard>> {
    public List<CalendarCard> calendarEvents;
    public CalendarCardAdapter adapter;
    public Context context;
    public SwipeRefreshLayout swipeContainer;

    @Override
    protected void onPreExecute() {
        // Show spinning update arrow when first run
        swipeContainer.post(new Runnable() {
            @Override
            public void run() {
                swipeContainer.setRefreshing(true);
            }
        });
    }

    @Override
    protected List<CalendarCard> doInBackground(CalendarSettings... settings) {
        int counter = 0; // Count number of elements
        int maxElements = settings[0].maxElements;
        boolean oldEvents = settings[0].oldEvents;
        String urlString = settings[0].url;

        List<CalendarCard> parsedCalendar = new ArrayList<>(); // Initialise calendar

        DateFormat parseFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'", Locale.GERMANY);
        DateFormat outputFormatTime = new SimpleDateFormat("HH:mm",Locale.GERMANY);
        DateFormat outputFormatDate = new SimpleDateFormat("dd/MM/yyyy");
        parseFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        outputFormatTime.setTimeZone(TimeZone.getTimeZone("Europe/Stockholm"));

        try {
            System.setProperty("ical4j.parsing.relaxed", "true");
            URL url = new URL(urlString); // We should only get one url
            Calendar calendar = Calendars.load(url);

            Date today = new Date();
            /** Loop through Calendar **/
            for (Iterator k = calendar.getComponents().iterator(); k.hasNext();) {
                Component component = (Component) k.next();


                /** Store start and end date **/
                Property dtstart = component.getProperties().getProperty(Property.DTSTART);
                Property dtend = component.getProperties().getProperty(Property.DTEND);
                Property summary = component.getProperties().getProperty(Property.SUMMARY);
                Property description = component.getProperties().getProperty(Property.DESCRIPTION);
                Property location = component.getProperties().getProperty(Property.LOCATION);

                try {
                    Date start_date = parseFormat.parse(dtstart.getValue());
                    Date event_time = parseFormat.parse(dtend.getValue());
                    /** Only display events after today **/
                    if(oldEvents || start_date.after(today)) {
                        String sdtstart = context.getResources().getString(R.string.calendar_date) + outputFormatDate.format(start_date);
                        String stime = context.getResources().getString(R.string.calendar_time) + outputFormatTime.format(start_date) + context.getResources().getString(R.string.calendar_to) + outputFormatTime.format(event_time);
                        String slocation = context.getResources().getString(R.string.calendar_location) + location.getValue();
                        String sdescription = description.getValue();
                        if(sdescription.isEmpty()) {
                            sdescription = context.getResources().getString(R.string.no_description);
                        }
                        parsedCalendar.add(new CalendarCard(summary.getValue(), sdtstart, stime, slocation, sdescription));

                        if(++counter >= maxElements) {
                            break; // End for-loop
                        }
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        return parsedCalendar;
    }

    @Override
    protected void onPostExecute(List<CalendarCard> parsedCalendar) {
            // Remove all old elements before the new ones are added to avoid duplicates
            calendarEvents.clear();
            calendarEvents.addAll(parsedCalendar);

        // If we used pull to refresh, remove the spinning update arrow
        if(swipeContainer != null) {
            swipeContainer.setRefreshing(false);
        }

        // Update recycler view
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onCancelled(List<CalendarCard> parsedCalendar) {
        if(swipeContainer != null) {
            swipeContainer.setRefreshing(false);
        }

        if(calendarEvents.isEmpty()) {
            calendarEvents.add(new CalendarCard(context.getResources().getString(R.string.no_news),
                    context.getResources().getString(R.string.no_news_explanation), null, null, null));
        }

        // Update recycler view
        adapter.notifyDataSetChanged();
    }
}