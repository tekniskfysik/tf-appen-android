package se.tekniskfysik.utils;

public class CalendarSettings {
    int maxElements;
    boolean oldEvents;
    String url;

    public CalendarSettings(int maxElements, boolean oldEvents, String url) {
        this.maxElements = maxElements;
        this.oldEvents = oldEvents;
        this.url = url;
    }
}