package se.tekniskfysik.utils;

public class RSSSettings {
    int maxElements;
    String url;

    public RSSSettings(int maxElements, String url) {
        this.maxElements = maxElements;
        this.url = url;
    }
}
