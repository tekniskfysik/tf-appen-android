package se.tekniskfysik.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import se.tekniskfysik.R;


public class songFragment extends Fragment {


    public songFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();

        // Set actionbar title
        String title = getContext().getString(R.string.nav_songs);
        getActivity().setTitle(title);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_song, container, false);

        // Recycler view and cards
        RecyclerView rView = (RecyclerView) rootView.findViewById(R.id.song_recycler_view);
        rView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));

        // Create a list and set the adapter
        List<Song> song = new ArrayList<>();
        songAdapter adapter = new songAdapter(song);
        rView.setAdapter(adapter);

        // Add song name to the cards
        String[][] songs = getSongs();
        for (String[] songLoop : songs) {
            song.add(new Song(R.drawable.ic_queue_music_black_48dp, songLoop[0], songLoop[1]));
        }

        return rootView;
    }

    private String[][] getSongs() {
        String[] songNames = getResources().getStringArray(R.array.song_names);
        String[] lyrics = getResources().getStringArray(R.array.lyrics);

        // Column 1: song names. Column 2: lyrics
        String[][] songs = new String[songNames.length][2];

        for(int i = 0; i < songNames.length; i++) {
            songs[i][0] = songNames[i];
            songs[i][1] = lyrics[i];
        }

        return songs;
    }
}