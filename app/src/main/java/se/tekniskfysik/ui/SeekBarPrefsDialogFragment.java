package se.tekniskfysik.ui;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceDialogFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import se.tekniskfysik.R;

public class SeekBarPrefsDialogFragment extends PreferenceDialogFragmentCompat
        implements SeekBar.OnSeekBarChangeListener {

    private SeekBar seek;
    private TextView seekValue;
    private int seekProgress;

    // Populated by newInstance(), which is called from settingsFragment
    private static String key;
    private static SeekBarPrefs prefs;


    @Override
    protected void onBindDialogView(View view) {
        super.onBindDialogView(view);

        // Get the seekbar and set current progress as well as a listener
        seek = (SeekBar) view.findViewById(R.id.seek);
        seekValue = (TextView) view.findViewById(R.id.seek_value);

        // Get saved settings from sharedPreferences
        // Default value of 25 just to set something. Should already be set by MainActivity on first launch
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        seekProgress = sharedPrefs.getInt(key, 25);

        seek.setProgress(seekProgress);
        seekValue.setText(String.valueOf(seekProgress));

        // Must be set after setProgress() and setText() have been called in order not to trigger
        // onProgressChanged()
        seek.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onDialogClosed(boolean b) {
        if(b) {
            prefs.saveSharedPreferences(seekProgress);
        }
    }

    // Called by settingsFragment
    public static SeekBarPrefsDialogFragment newInstance(Preference preference) {
        key = preference.getKey();
        prefs = (SeekBarPrefs) preference;
        SeekBarPrefsDialogFragment fragment = new SeekBarPrefsDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString("key", key);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        // +1 in order to force > 0
        seekProgress = progress + 1;
        seekValue.setText(String.valueOf(seekProgress));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}