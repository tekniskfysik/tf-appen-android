package se.tekniskfysik.ui;

class Song {
    int pic;
    String songName;
    String lyrics;

    Song(int pic, String songName, String lyrics) {
        this.pic = pic;
        this.songName = songName;
        this.lyrics = lyrics;
    }
}