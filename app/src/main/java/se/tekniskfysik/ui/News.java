package se.tekniskfysik.ui;

public class News {
    String headLine;
    String contents;
    String date;
    String url;

    public News(String headLine, String contents, String date, String url) {
        this.headLine = headLine;
        this.contents = contents;
        this.date = date;
        this.url = url;
    }
}