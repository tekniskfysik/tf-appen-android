package se.tekniskfysik.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import se.tekniskfysik.R;

public class newsAdapter extends RecyclerView.Adapter<newsAdapter.newsViewHolder> {
    public class newsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView newsHeadline;
        public TextView newsContents;
        public TextView newsDate;
        public CardView cView;

        newsViewHolder(View v) {
            super(v);
            newsHeadline = (TextView) v.findViewById(R.id.news_headline);
            newsContents = (TextView) v.findViewById(R.id.news_contents);
            newsDate = (TextView) v.findViewById(R.id.news_date);
            cView = (CardView) v.findViewById(R.id.card_view);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();

            // Check if there is a link
            if(newsList.get(position).url != null) {
                // Open browser or other app
                Context context = cView.getContext();
                Intent browserIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(newsList.get(position).url));
                context.startActivity(browserIntent);
            }
        }
    }

    private List<News> newsList;
    newsAdapter(List<News> list) {
        this.newsList = list;
    }

    @Override
    public newsAdapter.newsViewHolder onCreateViewHolder(ViewGroup grp, int i) {
        View view = LayoutInflater.from(grp.getContext()).inflate(R.layout.news_card_layout, grp, false);
        return new newsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final newsViewHolder newsViewHolder, final int i) {
        newsViewHolder.newsHeadline.setText(newsList.get(i).headLine);
        newsViewHolder.newsContents.setText(newsList.get(i).contents);
        newsViewHolder.newsDate.setText(newsList.get(i).date);
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }
}