package se.tekniskfysik.ui;


import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import se.tekniskfysik.R;
import se.tekniskfysik.utils.RSSFeedAsyncTask;
import se.tekniskfysik.utils.RSSSettings;


public class newsContentFragment extends Fragment {
    // Create a list and an adapter for Async
    private List<News> news = new ArrayList<>();
    private newsAdapter adapter = new newsAdapter(news);

    // Pull to refresh
    private SwipeRefreshLayout swipeContainer;

    public newsContentFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();

        // Set actionbar title
        String title = getContext().getString(R.string.nav_twitter);
        getActivity().setTitle(title);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_news_content, container, false);

        // Recycler view and cards
        RecyclerView rView = (RecyclerView) rootView.findViewById(R.id.news_recycler_view);

        // Choose different layouts for large (>= 600 dp) and small screens
        if(getResources().getConfiguration().screenWidthDp >= 600) {
            rView.setLayoutManager(new StaggeredGridLayoutManager(2,
                    StaggeredGridLayoutManager.VERTICAL));
        } else {
            rView.setLayoutManager(new StaggeredGridLayoutManager(1,
                    StaggeredGridLayoutManager.VERTICAL));
        }

        rView.setAdapter(adapter);

        // Set up the swipe container
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.news_swipe_container);
        swipeContainer.setColorSchemeResources(R.color.colorPrimary);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRss(); // Update the feed
            }
        });

        getRss(); // Get RSS feed

        return rootView;
    }

    private void getRss() {
        // Setup things in the RSSFeedAsyncTask class
        RSSFeedAsyncTask RSSFeedAsync = new RSSFeedAsyncTask();
        RSSFeedAsync.context = getContext();
        RSSFeedAsync.adapter = adapter;
        RSSFeedAsync.news = news;
        RSSFeedAsync.swipeContainer = swipeContainer;

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        int maxElements = sharedPrefs.getInt("prefs_feed_max_twitter", 25);
        RSSSettings feedSettings = new RSSSettings(maxElements, "https://twitrss.me/twitter_user_to_rss/?user=tekniskfysik");

        // Check if we have access to network
        ConnectivityManager conMgr =  (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo connection = conMgr.getActiveNetworkInfo();
        if(connection == null) {
            // No internet => don't try to download
            RSSFeedAsync.cancel(true);
        } else {
            // We have internet, get the feed
            RSSFeedAsync.execute(feedSettings);
        }
    }
}
