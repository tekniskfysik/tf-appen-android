package se.tekniskfysik.ui;

public class CalendarCard {
    String summary;
    String startdate;
    String time;
    String location;
    String description;

    public CalendarCard(String summary, String startdate,String time,String location,String description) {
        this.summary = summary;
        this.startdate = startdate;
        this.time = time;
        this.location = location;
        this.description = description;
    }
}