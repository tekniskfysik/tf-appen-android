package se.tekniskfysik.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import se.tekniskfysik.R;

public class CalendarCardAdapter extends RecyclerView.Adapter<CalendarCardAdapter.newsViewHolder> {
    public class newsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView cardSummary;
        public TextView cardStartDate;
        public TextView cardTime;
        public TextView cardLocation;
        public CardView cView;

        newsViewHolder(View v) {
            super(v);
            cardSummary = (TextView) v.findViewById(R.id.card_summary);
            cardStartDate = (TextView) v.findViewById(R.id.card_startdate);
            cardTime = (TextView) v.findViewById(R.id.card_time);
            cardLocation = (TextView) v.findViewById(R.id.card_location);
            cView = (CardView) v.findViewById(R.id.card_view);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();

            Context context = cView.getContext();
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setMessage(cardList.get(position).description);
            builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel(); // Cancel dialog when user presses "OK"
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }

    private List<CalendarCard> cardList;
    CalendarCardAdapter(List<CalendarCard> list) {
        this.cardList = list;
    }

    @Override
    public CalendarCardAdapter.newsViewHolder onCreateViewHolder(ViewGroup grp, int i) {
        View view = LayoutInflater.from(grp.getContext()).inflate(R.layout.calendarcard_layout, grp, false);
        return new newsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final newsViewHolder newsViewHolder, final int i) {
        newsViewHolder.cardSummary.setText(cardList.get(i).summary);
        newsViewHolder.cardStartDate.setText(cardList.get(i).startdate);
        newsViewHolder.cardTime.setText(cardList.get(i).time);
        newsViewHolder.cardLocation.setText(cardList.get(i).location);
    }

    @Override
    public int getItemCount() {
        return cardList.size();
    }
}