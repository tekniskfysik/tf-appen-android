package se.tekniskfysik.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v7.preference.DialogPreference;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.AttributeSet;
import android.widget.SeekBar;

import se.tekniskfysik.R;

public class SeekBarPrefs extends DialogPreference {
    private SeekBar seek;
    private Integer currentVal;
    private Integer newVal;
    private int DEFAULT_VALUE = 25; // Arbitrary. Should have been set by MainActivity on first launch

    public SeekBarPrefs(Context context, AttributeSet attrs) {
        super(context, attrs);
        seek = new AppCompatSeekBar(context, attrs);

        setDialogLayoutResource(R.layout.prefs_seekbar);
        setPositiveButtonText(R.string.dialog_ok);
        setNegativeButtonText(R.string.dialog_cancel);
    }

    // Called from SeekBarPrefsDialogFragment when the user presses OK in the dialog
    public void saveSharedPreferences(Integer num) {
        persistInt(num);
    }

    @Override
    protected void onSetInitialValue(boolean restorePersistedValue, Object defaultValue) {
        if (restorePersistedValue) {
            // Restore existing state
            currentVal = this.getPersistedInt(DEFAULT_VALUE);
        } else {
            // Set default state from the XML attribute
            currentVal = (Integer) defaultValue;
            persistInt(currentVal);
        }
    }

    // Get the default value from the XML attributes
    @Override
    protected Object onGetDefaultValue(TypedArray a, int index) {
        return a.getInteger(index, DEFAULT_VALUE);
    }

    // Handle activity lifecycle
    private static class SavedState extends BaseSavedState {
        int value;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        public SavedState(Parcel source) {
            super(source);
            // Get the current preference's value
            value = source.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            // Write the preference's value
            dest.writeInt(value);
        }

        public static final Parcelable.Creator<SavedState> CREATOR =
                new Parcelable.Creator<SavedState>() {

                    public SavedState createFromParcel(Parcel in) {
                        return new SavedState(in);
                    }

                    public SavedState[] newArray(int size) {
                        return new SavedState[size];
                    }
                };
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        final Parcelable superState = super.onSaveInstanceState();
        // Check whether this Preference is persistent (continually saved)
        if (isPersistent()) {
            // No need to save instance state since it's persistent,
            // use superclass state
            return superState;
        }

        // Create instance of custom BaseSavedState
        final SavedState savedState = new SavedState(superState);
        // Set the state's value with the class member that holds current
        // setting value
        savedState.value = newVal;
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state == null || !state.getClass().equals(SavedState.class)) {
            // Didn't save the state, so call superclass
            super.onRestoreInstanceState(state);
            return;
        }

        // Cast state to custom BaseSavedState and pass to superclass
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());

        seek.setProgress(savedState.value);
    }
}
