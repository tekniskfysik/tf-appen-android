package se.tekniskfysik.ui;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import se.tekniskfysik.R;

public class songAdapter extends RecyclerView.Adapter<songAdapter.songViewHolder> {
    public class songViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView tonePic;
        public TextView songName;
        public CardView cView;

        songViewHolder(View v) {
            super(v);
            tonePic = (ImageView) v.findViewById(R.id.tone_pic);
            songName = (TextView) v.findViewById(R.id.song_name);
            cView = (CardView) v.findViewById(R.id.song_card_view);
            cView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();

            Context context = cView.getContext();

            // Send the song name and lyrics to songActivity
            Intent intent = new Intent(context, songActivity.class);
            intent.putExtra(MainActivity.SONG_NAME, songList.get(position).songName);
            intent.putExtra(MainActivity.LYRICS, songList.get(position).lyrics);
            context.startActivity(intent);
            }
        }

    private List<Song> songList;
    songAdapter(List<Song> list) {
        this.songList = list;
    }

    @Override
    public songAdapter.songViewHolder onCreateViewHolder(ViewGroup grp, int i) {
        View view = LayoutInflater.from(grp.getContext()).inflate(R.layout.song_card_layout, grp, false);
        return new songViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final songViewHolder songViewHolder, final int i) {
        songViewHolder.tonePic.setImageResource(songList.get(i).pic);
        songViewHolder.songName.setText(songList.get(i).songName);
    }

    @Override
    public int getItemCount() {
        return songList.size();
    }
}