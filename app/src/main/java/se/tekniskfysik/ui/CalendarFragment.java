package se.tekniskfysik.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import se.tekniskfysik.R;
import se.tekniskfysik.utils.CalendarAsyncTask;
import se.tekniskfysik.utils.CalendarSettings;


public class CalendarFragment extends Fragment {
    // Create a list and an adapter for Async
    private List<CalendarCard> calendarEvents = new ArrayList<>();
    private CalendarCardAdapter adapter = new CalendarCardAdapter(calendarEvents);

    // Pull to refresh
    private SwipeRefreshLayout swipeContainer;

    public CalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();

        // Set actionbar title
        String title = getContext().getString(R.string.nav_cal);
        getActivity().setTitle(title);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);
        RecyclerView rView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        if (getResources().getConfiguration().screenWidthDp >= 600) {
            rView.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        } else {
            rView.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL));
        }

        rView.setAdapter(adapter);

        // Set up the swipe container
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.calendar_swipe_container);
        swipeContainer.setColorSchemeResources(R.color.colorPrimary);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getCalendarEvents(); // Update the calendar events
            }
        });

        getCalendarEvents();

        return rootView;
    }

    private void getCalendarEvents() {
        // Setup things in the CalendarAsyncTask class
        CalendarAsyncTask CalendarAsync = new CalendarAsyncTask();
        CalendarAsync.calendarEvents = calendarEvents;
        CalendarAsync.context = getContext();
        CalendarAsync.adapter = adapter;
        CalendarAsync.swipeContainer = swipeContainer;

        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        int maxElements = sharedPrefs.getInt("prefs_feed_max_calendar", 25);
        boolean oldEvents = sharedPrefs.getBoolean("prefs_old_calendar_events", true);
        CalendarSettings calSettings = new CalendarSettings(maxElements, oldEvents, "https://www.google.com/calendar/ical/6ensdcblttc8o97tsii76tia1c%40group.calendar.google.com/public/basic.ics");

        // Check if we have access to network
        ConnectivityManager conMgr =  (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo connection = conMgr.getActiveNetworkInfo();
        if(connection == null) {
            // No internet => don't try to download
            CalendarAsync.cancel(true);
        } else {
            // We have internet, get the calendar events
            CalendarAsync.execute(calSettings);
        }
    }
}
