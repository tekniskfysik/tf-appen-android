package se.tekniskfysik.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.Random;

import se.tekniskfysik.BuildConfig;
import se.tekniskfysik.R;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    // Used in songAdapter to pass songs to songActivity
    public final static String SONG_NAME = "se.tekniskfysik.ui.MainActivity.SONG_NAME";
    public final static String LYRICS = "se.tekniskfysik.ui.MainActivity.LYRICS";

    // Used to get quotes by shaking
    private SensorManager mSensorManager;
    private ShakeEventListener mSensorListener;
    private String[] myString;
    private static final Random rgenerator = new Random();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set default settings. "false" makes sure this only happens the first time the app is run
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        NavigationView nView = (NavigationView) findViewById(R.id.nav_view);
        assert nView != null;
        nView.setNavigationItemSelectedListener(this);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        assert drawer != null;
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        if(savedInstanceState == null) {
            // Show news fragment by default
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.frame_content,
                    new newsContentFragment()).commit();
        }

        /** Declare an array with quotes. Quotes are found in res/values/array.xml */
        Resources res = getResources();
        myString = res.getStringArray(R.array.Quotes);

        /** Create a shake listener */
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorListener = new ShakeEventListener();
        mSensorListener.setOnShakeListener(new ShakeEventListener.OnShakeListener() {
            public void onShake() {
                /** Pick a random quote from the array */
                String q = myString[rgenerator.nextInt(myString.length)];
                /** Write quote on screen */
                Toast.makeText(MainActivity.this, q, Toast.LENGTH_LONG).show();
                Toast.makeText(MainActivity.this, q, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        // Show about dialog
        if (id == R.id.action_about) {
            // Get app name and version number
            String appName = getResources().getString(R.string.app_name);
            String appVersion = BuildConfig.VERSION_NAME;
            String aboutMsg = String.format(getResources().getString(R.string.about_msg),
                    appName, appVersion);

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle(R.string.dotmenu_about);
            builder.setMessage(aboutMsg);
            builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel(); // Cancel dialog when user presses "OK"
                }
            });

            // Create and show dialog
            AlertDialog dialog = builder.create();
            dialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        Fragment fragment = null;
        Class fragmentClass;

        switch(id) {
            case R.id.nav_twitter:
                fragmentClass = newsContentFragment.class;
                break;
            case R.id.nav_instagram:
                fragmentClass = InstagramFragment.class;
                break;
            case R.id.nav_cal:
                fragmentClass = CalendarFragment.class;
                break;
            case R.id.nav_origo:
                fragmentClass = OrigoFragment.class;
                break;
            case R.id.nav_songs:
                fragmentClass = songFragment.class;
                break;
            case R.id.nav_contact:
                fragmentClass = contactFragment.class;
                break;
            case R.id.nav_settings:
                fragmentClass = settingsFragment.class;
                break;
            default:
                fragmentClass = newsContentFragment.class;
                break;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            Log.e("Fragment problem", "Fragment could not be loaded", e);
        }

        // Show new fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_content, fragment).addToBackStack(null).commit();

        // Mark the new item
        item.setChecked(true);


        // Close drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        mSensorManager.registerListener(mSensorListener,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    public void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(mSensorListener);
    }
}