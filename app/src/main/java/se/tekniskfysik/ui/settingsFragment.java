package se.tekniskfysik.ui;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;

import se.tekniskfysik.R;

public class settingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onStart() {
        super.onStart();

        // Set actionbar title
        String title = getContext().getString(R.string.nav_settings);
        getActivity().setTitle(title);
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {
        addPreferencesFromResource(R.xml.preferences);
    }

    @Override
    public void onDisplayPreferenceDialog(Preference preference) {
        DialogFragment fragment;
        if (preference instanceof SeekBarPrefs) {
            fragment = SeekBarPrefsDialogFragment.newInstance(preference);
            fragment.setTargetFragment(this, 0);
            fragment.show(getFragmentManager(), "android.support.v7.preference.PreferenceFragment.DIALOG");
        } else {
            super.onDisplayPreferenceDialog(preference);
        }
    }
}
