package se.tekniskfysik.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

import se.tekniskfysik.R;

public class songActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // The toolbar will always be here
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        String songName = intent.getStringExtra(MainActivity.SONG_NAME);
        String songLyrics = intent.getStringExtra(MainActivity.LYRICS);

        TextView name = (TextView) findViewById(R.id.song_activity_name);
        TextView lyrics = (TextView) findViewById(R.id.song_activity_lyrics);

        name.setText(songName);
        lyrics.setText(songLyrics);

        lyrics.setMovementMethod(new ScrollingMovementMethod());
    }
}