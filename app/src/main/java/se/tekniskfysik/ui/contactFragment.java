package se.tekniskfysik.ui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import se.tekniskfysik.R;

public class contactFragment extends Fragment {


    public contactFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();

        // Set actionbar title
        String title = getContext().getString(R.string.nav_contact);
        getActivity().setTitle(title);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_contact, container, false);
        TextView contactText = (TextView) rootView.findViewById(R.id.contact_textview);

        CharSequence styledText = Html.fromHtml(getResources().getString(R.string.contact_info));
        contactText.setText(styledText);

        return rootView;
    }

}